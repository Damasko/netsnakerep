extends Node2D


onready var players = [];
var background = null;

export(Color) var color1 = Color(0.8, 0.3, 0.1, 1.0);
export(Color) var color2 = Color(0.3, 0.8, 0.1, 1.0);
export(Color) var color3 = Color(0.3, 0.1, 0.8, 1.0);
export(Color) var color4 = Color(0.3, 0.8, 0.6, 1.0);

var texPos = Vector2();

func _ready():
	
	background = $draw;
	
	players.push_back($Player);

	background.get_material().set_shader_param("lightCol1", color1);
	$Player.set_color(color1);
	
	if $AIPlayer.enabled:
		players.push_back($AIPlayer);
		background.get_material().set_shader_param("lightCol2", color2);
		$AIPlayer.set_color(color2);
	
	if $AIPlayer2.enabled:
		players.push_back($AIPlayer2);
		background.get_material().set_shader_param("lightCol3", color3);
		$AIPlayer2.set_color(color3);
	
	if $AIPlayer3.enabled:
		players.push_back($AIPlayer3);
		background.get_material().set_shader_param("lightCol4", color4);
		$AIPlayer3.set_color(color4);
	
	"""
	print(players[0].get_node("Data").get_color());
	
	for i in range(players.size()):
		
		players[i].get_node("Data").set_color(players[i].get_node("Data").get_color());
		background.get_material().set_shader_param("lightCol" + str(i+1), 
										players[i].get_node("Data").get_color());
	"""
	
	texPos = Vector2($draw.texture.get_size() / 2);
	texPos.x -= 10;
	
	for player in players:
		player.texPos = texPos;

	background.get_material().set_shader_param("texPosition", 	texPos);
	
	set_process(true);
	
func _process(delta):
	
	for i in range(players.size()):
		#print("Player " + str(i) + "  pos: " + str(playerPositions[i].get_position()));
		background.get_material().set_shader_param("lightPos" + str(i+1), 
										players[i].get_global_position());
										
	
	"""
	var c = $Player.color;
	c.r += sin(delta) * 0.1;
	c.g += sin(delta) * 0.1;
	if c.r >= 1:
		c.r = 0;
	if c.g >= 1:
		c.g = 0;
	print(c);
	background.get_material().set_shader_param("lightCol1", 
										c);
				
	$Player.set_color(c);
	"""

	
func _draw():
	
	for player in players:
		draw_circle(player.futurePos, 20, player.data.get_color());

