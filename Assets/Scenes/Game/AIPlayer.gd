extends "res://MoveBase.gd"

var particles = null;

var player = null;
var lastPlayerPos = Vector2(0, 0);

var line = null;
var previousFacingVec = Vector2(0, 0);
var data = null;


var minDistToTarget = 100;

export var enabled = false;

export(Color) var color;

func _ready():
	
	particles = $Particles2D;
	line = $Line;
	line.set_as_toplevel(true);
	#set_process(true);
	set_physics_process(true);
	data = $Data;
	
	targetPoint.x = (randi()%1000) - 500;
	targetPoint.y = (randi()%500) - 200;

	#SPEED = SPEED - SPEED / 4;
	#ROTATION_FORCE = ROTATION_FORCE * 3;

	
func _physics_process(delta):
	
	if not enabled:
		return;
	
	var distTT = targetPoint.distance_to(get_global_position());

	if distTT < minDistToTarget:
		
		targetPoint.x = (randi()%1000) - 500;
		targetPoint.y = (randi()%500) - 200;
		get_parent().update();
		
	var dest = (targetPoint - get_global_position()).normalized();

	moveForward(delta);

	var angle = dest.angle_to(-facingVec.normalized());
	
	angle = rad2deg(angle);
	angle = rad2deg(angle);

	if angle > 179.5:
		turnRight();
	elif angle < 180.5:
		turnLeft();
		
		
	if outOfScreen:
		line.clear_points();
	
	if line.points.size() > maxPoints:
		line.remove_point(0);
		
	line.add_point(get_global_position());
	
		
func set_color(newColor):
	
	color = newColor;
	$Particles2D.modulate = color;
	line.modulate = color;
	
