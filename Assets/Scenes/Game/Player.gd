extends "res://MoveBase.gd"

export(Color) var color;


var line = null;
var data = null;

func _ready():

	set_physics_process(true);
	data = $Data;
	line = $Line;
	line.set_as_toplevel(true);
	color = data.color;

func _physics_process(delta):
	
	if (Input.is_action_pressed("accelerate")):
		
		moveForward(delta);
		
	if (Input.is_action_pressed("break")):
		
		moveBackwards(delta);
	
	if (Input.is_action_pressed("turn_left")):	
	
		turnLeft();
		
	if (Input.is_action_pressed("turn_right")):
		
		turnRight();
	
	if outOfScreen:
		line.clear_points();
	
	if line.points.size() > maxPoints:
		line.remove_point(0);
		
	line.add_point(get_global_position());
	

func set_color(newColor):
	
	color = newColor;
	$Particles2D.modulate = color;
	line.modulate = color;

