extends Node2D

var SPEED = 1200.0;
var MAX_SPEED = 1200.0;
var MASS = 2.0;
var ROTATION_FORCE = 3.0;
var steering = Vector2();
var targetMotion = Vector2();
var motion = Vector2();
var finalMotion = Vector2();
var light = null;
var forward = false;

func _ready():

	set_physics_process(true);
	light = get_node("Light2D");
	
func _physics_process(delta):
	
	finalMotion = Vector2();
	forward = false;

func moveForward(delta):

	var rotation = get_transform().get_rotation() - PI / 2.0;
	var facingVec = Vector2(cos(rotation), sin(rotation));
	facingVec.x = deg2rad(facingVec.x);
	facingVec.y = deg2rad(facingVec.y);

	facingVec = facingVec.normalized();

	#var speedX = clamp(facingVec.x * SPEED * delta, 0, MAX_SPEED);
	#var speedY = clamp(facingVec.y * SPEED * delta, 0, MAX_SPEED);
	
	var speedX = facingVec.x * SPEED;
	var speedY = facingVec.y * SPEED;
	
	var targetMotion = Vector2(speedX, speedY);
	
	steering = targetMotion - motion

	if steering.length() > 1:
		steering = steering.normalized();
	
	motion += steering / MASS;
	
	if motion.length() <= 1:
		motion = Vector2();

	finalMotion = targetMotion - motion

	var col = move_and_slide(finalMotion);
	forward = true;

#	if (col):
#		var reflect = col.remainder.bounce(col.normal);
#		reflect = reflect.normalized();
#		rotate(atan2(reflect.x, reflect.y));

func moveBackwards(delta):

	var rotation = get_transform().get_rotation() - PI / 2.0;
	var facingVec = Vector2(cos(rotation), sin(rotation));
	facingVec.x = deg2rad(facingVec.x);
	facingVec.y = deg2rad(facingVec.y);

	facingVec = facingVec.normalized();

	#var speedX = clamp(facingVec.x * SPEED * delta, 0, MAX_SPEED);
	#var speedY = clamp(facingVec.y * SPEED * delta, 0, MAX_SPEED);
	
	var speedX = facingVec.x * SPEED;
	var speedY = facingVec.y * SPEED;
	
	var targetMotion = Vector2(speedX, speedY);
	
	steering = targetMotion - motion

	if steering.length() > 1:
		steering = steering.normalized();
	
	motion += steering / MASS;
	
	if motion.length() <= 1:
		motion = Vector2();

	finalMotion = -targetMotion - motion

	var col = move_and_slide(finalMotion / 2);

#	if (col):
#		var reflect = col.remainder.bounce(col.normal);
#		reflect = reflect.normalized();
#		rotate(atan2(reflect.x, reflect.y));

func turnLeft():

	if finalMotion.length() > 0:
		if forward:
			rotate(-ROTATION_FORCE * get_process_delta_time())
		else:
			rotate(ROTATION_FORCE * get_process_delta_time())

func turnRight():

	if finalMotion.length() > 0:
		if forward:
			rotate(ROTATION_FORCE * get_process_delta_time());
		else:
			rotate(-ROTATION_FORCE * get_process_delta_time())
