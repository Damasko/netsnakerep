extends KinematicBody2D

var data = null

func _ready():
	
	data = $Data;

func get_color():
	
	return data.get_color();
