extends Node2D

export(Color) onready var color;

func get_color():
	
	return color;

func set_color(newColor):
	
	color = newColor;
	get_parent().get_node("Particles2D").modulate = color;
	get_parent().get_node("Line").modulate = color;
