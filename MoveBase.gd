extends KinematicBody2D

var SPEED = 900.0;
var MAX_SPEED = 900.0;
var MASS = 1.0;
var ROTATION_FORCE = 7.0;
var steering = Vector2();
var targetMotion = Vector2();
var motion = Vector2();
var finalMotion = Vector2();
var facingVec = Vector2();

var targetPoint = Vector2();

var previousPos = Vector2();
var futurePos = Vector2();

var texPos = Vector2();

var forward = false;

var outOfScreen = false;

var maxPoints = 32;

func _ready():

	set_physics_process(true);
	
func _physics_process(delta):
	
	finalMotion = Vector2();
	texPos = get_parent().texPos;
	forward = false;

func moveForward(delta):

	outOfScreen = false;
	var rotation = get_transform().get_rotation() - PI / 2.0;
	facingVec = Vector2(cos(rotation), sin(rotation));
	facingVec.x = deg2rad(facingVec.x);
	facingVec.y = deg2rad(facingVec.y);

	facingVec = facingVec.normalized();
	
	futurePos = get_global_position() + (previousPos - get_global_position());

	var speedX = facingVec.x * SPEED * 0.001;
	var speedY = facingVec.y * SPEED * 0.001;
	
	var targetMotion = Vector2(speedX, speedY);
	
	steering = targetMotion - motion;
	
	if steering.length() > 1:
		steering = steering.normalized();
	
	motion += steering / MASS;
	
	if motion.length() <= 1:
		motion = Vector2();

	finalMotion = targetMotion - motion

	var col = move_and_collide(facingVec * SPEED * delta);
	forward = true;

	var pos = get_global_position();
	
	if pos.x < -510:
		pos.x = 510;
		outOfScreen = true;
	
	elif pos.x > 520:
		pos.x = -500;
		outOfScreen = true;
	
	if pos.y < -309:
		pos.y = 272;
		outOfScreen = true;
	
	elif pos.y > 290:
		pos.y = -307;
		outOfScreen = true;
	
	set_global_position(pos);

#	if (col):
#		var reflect = col.remainder.bounce(col.normal);
#		reflect = reflect.normalized();
#		rotate(atan2(reflect.x, reflect.y));

func moveBackwards(delta):

	var rotation = get_transform().get_rotation() - PI / 2.0;
	var facingVec = Vector2(cos(rotation), sin(rotation));
	facingVec.x = deg2rad(facingVec.x);
	facingVec.y = deg2rad(facingVec.y);

	facingVec = facingVec.normalized();

	#var speedX = clamp(facingVec.x * SPEED * delta, 0, MAX_SPEED);
	#var speedY = clamp(facingVec.y * SPEED * delta, 0, MAX_SPEED);
	
	var speedX = facingVec.x * SPEED;
	var speedY = facingVec.y * SPEED;
	
	var targetMotion = Vector2(speedX, speedY);
	
	steering = targetMotion - motion

	if steering.length() > 1:
		steering = steering.normalized();
	
	motion += steering / MASS;
	
	if motion.length() <= 1:
		motion = Vector2();

	finalMotion = -targetMotion - motion

	var col = move_and_slide(finalMotion / 2);
	
	previousPos = get_global_position();

#	if (col):
#		var reflect = col.remainder.bounce(col.normal);
#		reflect = reflect.normalized();
#		rotate(atan2(reflect.x, reflect.y));

func turnLeft():

	if finalMotion.length() > 0:
		if forward:
			rotate(-ROTATION_FORCE * get_process_delta_time())
		else:
			rotate(ROTATION_FORCE * get_process_delta_time())

func turnRight():

	if finalMotion.length() > 0:
		if forward:
			rotate(ROTATION_FORCE * get_process_delta_time());
		else:
			rotate(-ROTATION_FORCE * get_process_delta_time())
